/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomba;

/**
 *
 * @author Usuario
 */
public class Gasolina {
    private int idGasolina;
    private String tipo;
    private String marca;
    private float precio;
    
    //constructores

      public Gasolina(){
         this.idGasolina = 0;
        this.tipo = "";
        this.marca = "";
        this.precio = 0.0f;
    }
    
    
    public Gasolina(int idGasolina, String tipo, String marca, float precio) {
        this.idGasolina = idGasolina;
        this.tipo = tipo;
        this.marca = marca;
        this.precio = precio;
    }

    public int getIdGasolina() {
        return idGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public String getMarca() {
        return marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

   public String obtenerInformacion(){
       return "Id: " + this.idGasolina + 
              "Marca: " + this.marca + 
              "Tipo: " + this.tipo + 
              "Precio: " + this.precio;
   }
    
}
