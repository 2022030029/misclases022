/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomba;

/**
 *
 * @author Usuario
 */
public class Bomba {
    private int idBomba;
    private int capacidad;
    private int contadorLitros;
    private Gasolina gasolina;

    //constructores
    
    public Bomba(){
         this.idBomba = 0;
        this.capacidad = 0;
        this.contadorLitros = 0;
        this.gasolina = new Gasolina();
    }
    
    public Bomba(int idBomba, int capacidad, int contadorLitros, Gasolina gasolina) {
        this.idBomba = idBomba;
        this.capacidad = capacidad;
        this.contadorLitros = contadorLitros;
        this.gasolina = gasolina;
    }

    public int getIdBomba() {
        return idBomba;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public int getContadorLitros() {
        return contadorLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setIdBomba(int idBomba) {
        this.idBomba = idBomba;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public void setContadorLitros(int contadorLitros) {
        this.contadorLitros = contadorLitros;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    //Comportamiento
    public float obtenerInventario(){
        return this.capacidad - this.contadorLitros;
    }
    public float venderGasolina( int cantidad){
        float totalVenta= 0.0f;
        if(cantidad <= this.obtenerInventario()){
            totalVenta = cantidad*this.gasolina.getPrecio();
            this.contadorLitros = this.contadorLitros + cantidad;
        }
        return totalVenta;
    }
    public float totalVenta(){
        return this.contadorLitros * this.gasolina.getPrecio();
    }
    
}

